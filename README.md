CSC 591 Graph Data Mining - Project 4: Virus Propagation
==============================

Authors
------------------------

 + Chandan Dua [csdua]
 + Devendra D. Chavan [ddchavan]
 + Kandarp Srivastava [ksrivas]

Overview
----------------
A fundamental problem in epidemiology is determining whether a virus will 
result in an epidemic (that is, if it will rapidly spread to many people) or if 
it will die quickly. Recent works on virus propagation have approached this 
problem from a network perspective. Given a network of individuals, where the 
edges represent who can potentially infect whom, the rate of propagation of a 
virus across the network will depend on the connectivity of the network and on 
the propagation model of the virus. More precisely, researchers have determined 
that the strength of the virus can be defined as a function of the largest 
eigenvalue of the network’s adjacency matrix. These results have important 
implications for the design and evaluation of immunization policies, and can 
be applied to many problems, such as minimizing the spread of malware in 
computer networks or maximizing the spread of a marketing campaign across a 
social network.

Goal
----------------
Analyze the propagation of a virus in a network and prevent a network-wide 
epidemic. In order to do that, your team will need to:

 1. Analyze and understand a virus propagation model.
 2. Calculate the effective strength of a virus.
 3. Simulate the propagation of a virus in a network.
 4. Implement immunization policies to prevent a virus from spreading across a 
    network.

Required packages
----------------
The program requires the following packages:

 1. matplotlib
 2. numpy
 3. scipy
 4. igraph

These can be installed in Ubuntu (VCL - Ubuntu Base 12.04) using the following 
commands:

    sudo add-apt-repository ppa:igraph/ppa
    sudo apt-get update
    sudo apt-get install -y python-numpy python-scipy python-matplotlib python-igraph

DISPLAY environment variable for plot generation
-------------- 
Graph generation (`common.py:plot_graph_2D()`) requires an 
assosciated *DISPLAY* to be present on the system, hence it cannot be executed 
remotely via only `ssh` (for example, `ssh` into `Ubuntu VCL` instance). An 
error might be shown on the console if the program is unable to create the 
graphs.

    Traceback (most recent call last):
      File "simulate_static.py", line 27, in <module>
        'Delta={1}').format(BETA_1, DELTA_1))
      File "/home/ddchavan/src/common.py", line 11, in plot_graph_2D
        plt.clf()
      File "/usr/lib/pymodules/python2.7/matplotlib/pyplot.py", line 443, in clf
        gcf().clf()
      File "/usr/lib/pymodules/python2.7/matplotlib/pyplot.py", line 369, in gcf
        return figure()
      File "/usr/lib/pymodules/python2.7/matplotlib/pyplot.py", line 343, in figure
        **kwargs)
      File "/usr/lib/pymodules/python2.7/matplotlib/backends/backend_tkagg.py", line 80, in new_figure_manager
        window = Tk.Tk()
      File "/usr/lib/python2.7/lib-tk/Tkinter.py", line 1688, in __init__
        self.tk = _tkinter.create(screenName, baseName, className, interactive, wantobjects, useTk, sync, use)
    _tkinter.TclError: no display name and no $DISPLAY environment variable
    Command exited with non-zero status 1

Directory structure
-----------

    Project
    |--- README.md
    |--- Report.pdf
    |--- src - Contains the source scripts
    |    |--- vpm_static.py
    |    |--- vpm_alternating.py
    |    |--- simulate_static.py
    |    |--- simulate_alternating.py
    |    |--- immunization_static.py
    |    |--- immunization_alternating.py
    |    |--- run_all.sh
    |--- results- Containsthetraceoftheexecutionandplots
    |    |--- plots
    |    |    |--- *.png
    |    |--- trace
    |--- datasets
    |    |--- static.network
    |    |--- alternating1.network
    |    |--- alternating2.network
    
Usage
----------------

The generated graphs are present in the `results/plots` directory (assuming 
that the script is executed from the same directory as that containing the 
script i.e. `src`).

**1. Calculating effective strength of static network**

    > python vpm_static.py <network filepath>

For example,

    > python vpm_static.py ../datasets/static.network 
    Largest eigenvalue : 43.8546957607
    Strength for (beta, delta) = (0.2, 0.7) = 12.5299130745
    Min beta (for delta = 0.7) = 0.02, Strength = 1.25299130745
    Max delta (for beta = 0.2) = 0.99, Strength = 8.85953449712
    Strength for (beta, delta) = (0.01, 0.6) = 0.730911596012
    Min beta (for delta = 0.6) = 0.02, Strength = 1.46182319202
    Max delta (for beta = 0.01) = 0.43, Strength = 1.0198766456

**2. Running the simulation for static network**

    > python simulate_static.py <network filepath>

For example, 
    
    > python simulate_static.py ../datasets/static.network

    [simulation steps]
    ...

**3. Checking variation of strength with different policies for static network**

    > python immunization_static.py <network filepath>
    
For example,

    > python immunization_static.py ../datasets/static.network 
    (beta, delta) = (0.2, 0.7), K = 200
    [simulation steps]
    ...
    Strength Policy A : 11.895125617
    Strength Policy B : 1.08027548024
    Strength Policy C : 1.08452053952
    Strength Policy D : 3.53928970078

    Plotting k vs strength_A...
    Plotting k vs strength_B...
    Plotting k vs strength_C...
    Plotting k vs strength_D...

**4. Calculating effective strength of alternating network**

    > python vpm_alternating.py <dataset dirpath> <prefix>
    
For example,

    Largest eigenvalue for (beta, delta) = (0.2,0.7): 79.3061173276
    Strength for (beta, delta) = (0.2, 0.7) = 22.658890665
    Min beta (for delta = 0.7) = 0.0675, Strength = 1.00778764085
    Max delta (for beta = 0.2) = 0.9999, Strength = 15.2129698686
    Largest eigenvalue for (beta, delta) = (0.01,0.6): 0.703189808761
    Strength for (beta, delta) = (0.01, 0.6) = 0.011719830146
    Min beta (for delta = 0.6) = 0.0623, Strength = 1.01013502844
    Max delta (for beta = 0.01) = 0.0201, Strength = 1.00311708682


**5. Running the simulation for alternating network**

    > python simulate_alternating.py <dataset dirpath> <prefix>

For example, 
    
    > python simulate_alternating.py ../datasets alternating 

    [simulation steps]
    ...

**6. Checking variation of strength with different policies for alternating network**

    > python immunization_alternating.py <dataset dirpath> <prefix>

For example,

    > python immunization_alternating.py ../datasets alternating
    (beta, delta) = (0.2, 0.7), K = 200
    [simulation steps]
    ...
    Strength Policy A : 22.4038433065
    Strength Policy B : 0.31001582678
    Strength Policy C : 0.299574810536
    Strength Policy D : 21.0649364575
                                                                                  
    Plotting k vs strength_A...
    Plotting k vs strength_B...                                                   
    Plotting k vs strength_C...                                                   
    Plotting k vs strength_D...

Wrapper shell script
------------
A shell script that executes all the above python scripts is present in 
`src/run_all.sh`.

Execution time 
-------------
    
    vpm_static                  - 11.12s
    vpm_alternating             - 2h06m31s
    simulate_static             - 2m30.65s
    simulate_alternating        - 2m58.94s
    immunization_static         - 14m09.90s
    immunization_alternating    - 51m29.47s

Source code
-----------
The code is hosted on [bitbucket][1]

 [1]: https://bitbucket.org/devendra-d-chavan/graphdatamining_proj4
