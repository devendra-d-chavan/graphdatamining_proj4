#!/usr/bin/python

import igraph as ig
from scipy import sparse
from scipy.sparse import linalg
import numpy as np

from common import *

def apply_policy_A(graphs, k):
    # Policy A: Select k nodes at random for immunization
    import random
    
    # Remove k nodes from all the graphs
    trimmed_graphs = []
    
    vertices_to_remove = set()
    num_vertices = graphs[0].vcount()
    while(len(vertices_to_remove) < k):
        vertexid = random.randint(0, num_vertices - 1)
        vertices_to_remove.add(vertexid)
        
    for graph in graphs:
        graph.delete_vertices(ig.VertexSeq(graph, vertices_to_remove))
        trimmed_graphs.append(graph)
        
    return trimmed_graphs

def apply_policy_B(graphs, k):
    # Policy B: Select k highest degree nodes across all networks for 
    # immunization
    num_vertices = graphs[0].vcount()
    avg_node_degrees = [0] * num_vertices
    
    # Compute the average node degree
    # Initialize to zero
    for graph in graphs:
        i = 0
        for degree in graph.degree():
            avg_node_degrees[i] = avg_node_degrees[i] + degree           
            i = i + 1
     
    # Calculation of the average is not needed as the sum is a sufficient 
    # indicator of the order of the average node degree      
    # for avg_node_degree in avg_node_degrees:
    #    avg_node_degrees[i] = avg_node_degrees[i]/len(graphs)
    
    sorted_indices = np.argsort(avg_node_degrees)
    vertices_to_remove = \
                    [int(x) for x in sorted_indices[num_vertices - k:]]
    
    # Remove the vertices from all the graphs
    trimmed_graphs = []
    for graph in graphs:
        graph.delete_vertices(ig.VertexSeq(graph, vertices_to_remove))
        trimmed_graphs.append(graph)
        
    return trimmed_graphs

def apply_policy_C(graphs, k):
    # Policy C: Select highest degree node from all networks k times for
    # immunization
    trimmed_graphs = []
    
    # Select the nodes with the highest degree in any two of the networks
    i = 0
    while i < k:
        max_degree = -1   
        vertex_to_remove = -1  

        # if i != 0:
        #    graphs = trimmed_graphs
        #    trimmed_graphs = []

        for graph in graphs:
            node_degrees = graph.degree()
            
            # Get the max degree vertex id
            current_vertex_to_remove = \
                                np.argsort(node_degrees)[len(node_degrees) - 1]
            current_degree = node_degrees[vertex_to_remove]
            
            # Choose the vertex with the highest degree amongst all the graphs
            if current_degree > max_degree:
                max_degree = current_degree
                vertex_to_remove = current_vertex_to_remove
        
        # Remove the vertex from all the graphs
        vertices_to_remove = [int(vertex_to_remove)]
        for graph in graphs:
            graph.delete_vertices(ig.VertexSeq(graph, vertices_to_remove))
            trimmed_graphs.append(graph)
            
        i = i + 1
        graphs = trimmed_graphs
        trimmed_graphs = []
       
    return graphs

def apply_policy_D(graphs, beta, delta, k):
    # Policy D: Select the k highest corresponding nodes in the eigen vector 
    # corresponding to the largest eigen value
    sys_matrix = None
    first_term = (1 - delta) * sparse.identity(graphs[0].vcount())
    
    first_graph = True
    for graph in graphs:
        second_term = beta * get_sparse_matrix(graph)
        if first_graph:
            sys_matrix = first_term + second_term 
            first_graph = False
        else:
            sys_matrix = sys_matrix * (first_term + second_term)
     
    _, eigen_vector = linalg.eigsh(sys_matrix, k=1, which='LA',
                           return_eigenvectors=True)
    eigen_vector = [x[0] for x in eigen_vector]
     
    temp = eigen_vector
    temp.sort(reverse=True)
    
    # Get the k largest value
    k_largest_value = temp[k - 1]
    
    vertices_to_remove = []
    i = 0
    for i in range(0, len(eigen_vector)):
        if eigen_vector[i] >= k_largest_value:
            vertices_to_remove.append(i)
             
    trimmed_graphs = []
    for graph in graphs:
        graph.delete_vertices(ig.VertexSeq(graph, vertices_to_remove))
        trimmed_graphs.append(graph)
    
    return trimmed_graphs

def get_strength(graphs, beta, delta):
    import time

    # print 'Start strength : {0}'.format(time.clock())
    sys_matrix = None
    first_term = (1 - delta) * sparse.identity(graphs[0].vcount())
    
    sparse_matrices = []
    for graph in graphs:
        sparse_matrices.append(get_sparse_matrix(graph))
        
    first_graph = True
    for sparse_matrix in sparse_matrices:
        second_term = beta * sparse_matrix
        if first_graph:
            sys_matrix = first_term + second_term
            first_graph = False
        else:
            sys_matrix = sys_matrix * (first_term + second_term)
     
    eigen_value = linalg.eigsh(sys_matrix, k=1, which='LA',
                           return_eigenvectors=False)[-1]
   
    # print 'End strength : {0}'.format(time.clock())
 
    return eff_strength(eigen_value, beta, delta)

def step_immunization(dirpath, prefix, k, beta, delta, apply_policy,
                      run_simulation=False): 
    graphs = load_graphs(dirpath, prefix)
    
    if k > graphs[0].vcount():
        return 0
    
    if apply_policy == apply_policy_D:
        graphs = apply_policy(graphs, beta, delta, k)
    else:
        graphs = apply_policy(graphs, k)
        
    if run_simulation:
        policy_name = None
        if apply_policy == apply_policy_A:
            policy_name = 'Policy_A'
        elif apply_policy == apply_policy_B:
            policy_name = 'Policy_B'
        elif apply_policy == apply_policy_C:
            policy_name = 'Policy_C'
        elif apply_policy == apply_policy_D:
            policy_name = 'Policy_D'
    
        t = 100
        simulations = 10
        avg_percent_infected = simulate(graphs, beta, delta, t, simulations)
        plot_graph_2D(range(t), avg_percent_infected, 'simulation #',
                 'average_percent_infected', 'Average % infected with time',
                 ('Simulation_{0}_Immunized_Alternating_Avg_Percent_Infected_vs_' + 
                  'Time_For_Beta={2}_Delta={2}').format(policy_name, beta,
                                                        delta))
    strength = get_strength(graphs, beta, delta)
    return strength

def vary_k(dirpath, prefix, beta, delta, strength, apply_policy):
    NUM_INTERVALS = 20
    interval_size = 10
    interval_start = 0
    
    if strength > 1:
        # Try with 20 interval size
        interval_start = interval_start + NUM_INTERVALS * interval_size
        interval_size = 20
         
        strength = step_immunization(dirpath, prefix,
                                         interval_start + 
                                         NUM_INTERVALS * interval_size,
                                         beta, delta, apply_policy)
         
        if strength > 1:
            # Try with 50 interval size
            interval_start = interval_start + NUM_INTERVALS * interval_size
            interval_size = 50
            
            strength = step_immunization(dirpath, prefix,
                                         interval_start + 
                                         NUM_INTERVALS * interval_size,
                                         beta, delta, apply_policy)
            
            if strength > 1:
                # Try with 100 interval size
                interval_start = interval_start + NUM_INTERVALS * interval_size
                interval_size = 100
                
                strength = step_immunization(dirpath, prefix,
                                             interval_start + 
                                             NUM_INTERVALS * interval_size,
                                             beta, delta, apply_policy)
                
                if strength > 1:
                    # Try with 200 interval size
                    interval_start = (interval_start + 
                                      NUM_INTERVALS * interval_size)
                    interval_size = 200

    k_strength_tuples = [] 
    for k in range(interval_start, interval_start + 
                                    NUM_INTERVALS * interval_size + 1,
                                    interval_size):
        strength = step_immunization(dirpath, prefix, k, beta, delta,
                                     apply_policy)
        k_strength_tuples.append((k, strength))
        
    return k_strength_tuples

if __name__ == '__main__':
    import sys
    
    if len(sys.argv) < 3:
        print ('Usage: python immunization_alternating.py <dataset dirpath> ' + 
               '<prefix>')
        sys.exit()
        
    dirpath = sys.argv[1]
    prefix = sys.argv[2]
    
    K1 = 200
    BETA_1 = 0.2
    DELTA_1 = 0.7
    
    print '(beta, delta) = ({beta}, {delta}), K = {k}'.format(beta=BETA_1,
                                                              delta=DELTA_1,
                                                              k=K1)
    
    strength_A = step_immunization(dirpath, prefix, K1, BETA_1, DELTA_1,
                                   apply_policy_A, True)
    strength_B = step_immunization(dirpath, prefix, K1, BETA_1, DELTA_1,
                                   apply_policy_B, True)
    strength_C = step_immunization(dirpath, prefix, K1, BETA_1, DELTA_1,
                                   apply_policy_C, True)
    strength_D = step_immunization(dirpath, prefix, K1, BETA_1, DELTA_1,
                                   apply_policy_D, True)
    
    print (('Strength Policy A : {0}\nStrength Policy B : {1}\n' + 
            'Strength Policy C : {2}\nStrength Policy D : {3}\n')
           .format(strength_A, strength_B, strength_C, strength_D))
    
    print 'Plotting k vs strength_A...'
    k_strength_tuples = vary_k(dirpath, prefix, BETA_1, DELTA_1, strength_A,
                               apply_policy_A)
    plot_graph_2D([k for k, _ in k_strength_tuples],
                  [strength for _, strength in k_strength_tuples],
                  "k", "strength",
                  'Policy A - k v/s strength for $\\beta$ = ' + str(BETA_1) + 
                  ' and $\delta$ = ' + str(DELTA_1),
                  'Immunization_Alternating_PolicyA')
    
    print 'Plotting k vs strength_B...'
    k_strength_tuples = vary_k(dirpath, prefix, BETA_1, DELTA_1, strength_B,
                               apply_policy_B)
    plot_graph_2D([k for k, _ in k_strength_tuples],
                  [strength for _, strength in k_strength_tuples],
                  "k", "strength",
                  'Policy B - k v/s strength for $\\beta$ = ' + str(BETA_1) + 
                  ' and $\delta$ = ' + str(DELTA_1),
                  'Immunization_Alternating_PolicyB')
    
    print 'Plotting k vs strength_C...'
    k_strength_tuples = vary_k(dirpath, prefix, BETA_1, DELTA_1, strength_C,
                               apply_policy_C)
    plot_graph_2D([k for k, _ in k_strength_tuples],
                  [strength for _, strength in k_strength_tuples],
                  "k", "strength",
                  'Policy C - k v/s strength for $\\beta$ = ' + str(BETA_1) + 
                  ' and $\delta$ = ' + str(DELTA_1),
                  'Immunization_Alternating_PolicyC')
    
    print 'Plotting k vs strength_D...'
    k_strength_tuples = vary_k(dirpath, prefix, BETA_1, DELTA_1, strength_D,
                               apply_policy_D)
    plot_graph_2D([k for k, _ in k_strength_tuples],
                  [strength for _, strength in k_strength_tuples],
                  "k", "strength",
                  'Policy D - k v/s strength for $\\beta$ = ' + str(BETA_1) + 
                  ' and $\delta$ = ' + str(DELTA_1),
                  'Immunization_Alternating_PolicyD')    
