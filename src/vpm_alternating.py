#!/usr/bin/python

import igraph as ig
from scipy import sparse
from scipy.sparse import linalg
import numpy as np

from common import *

def step_alternating(sparse_matrices, num_vertices, beta, delta):
    import time
    
    sys_matrix = None
    first_term = (1 - delta) * sparse.identity(num_vertices)
    
    start = time.clock()
    first_graph = True
    for sparse_matrix in sparse_matrices:
        second_term = beta * sparse_matrix
        if first_graph:
            sys_matrix = first_term + second_term
            first_graph = False
        else:
            sys_matrix = sys_matrix * (first_term + second_term)
     
    end = time.clock()
    
    # print 'Time taken : {0}'.format(end - start)
    
    eigen_value = linalg.eigsh(sys_matrix, k=1, which='LA',
                           return_eigenvectors=False)[-1]
    print 'Largest eigenvalue for (beta, delta) = ({0},{1}): {2}'.format(
                                                                    beta,
                                                                    delta,
                                                                    eigen_value)
    
    return eff_strength(eigen_value, beta, delta) 

def get_beta_strengths(sparse_matrices, num_vertices, delta):
    
    first_term = (1 - delta) * sparse.identity(num_vertices)
    
    sys_matrix = None
    beta_strength_list = []
    
    for i in range(0, 10000):
        beta = 0.0001 * i
        first_graph = True  
        for sparse_matrix in sparse_matrices:
            second_term = beta * sparse_matrix
            if first_graph:
                sys_matrix = first_term + second_term
                first_graph = False
            else:
                sys_matrix = sys_matrix * (first_term + second_term)
                
        eigen_value = linalg.eigsh(sys_matrix, k=1, which='LA',
                                   return_eigenvectors=False)[-1]
        strength = eff_strength(eigen_value, beta, delta)
        beta_strength_list.append((beta, strength))
     
    return beta_strength_list  

def get_delta_strengths(sparse_matrices, num_vertices, beta):
    
    sys_matrix = None
    delta_strength_list = []
        
    iden_matrix = sparse.identity(num_vertices)
    for i in range(1, 10000):
        delta = 0.0001 * i
        first_term = (1 - delta) * iden_matrix
        
        first_graph = True  
        for sparse_matrix in sparse_matrices:
            second_term = beta * sparse_matrix
            if first_graph:
                sys_matrix = first_term + second_term
                first_graph = False
            else:
                sys_matrix = sys_matrix * (first_term + second_term)
                
        eigen_value = linalg.eigsh(sys_matrix, k=1, which='LA',
                                   return_eigenvectors=False)[-1]
        strength = eff_strength(eigen_value, beta, delta)
        delta_strength_list.append((delta, strength))
     
    return delta_strength_list   

def get_beta_delta_variations(sparse_matrices, num_vertices, beta, delta):
    beta_strength_list = get_beta_strengths(sparse_matrices, num_vertices,
                                            delta)
    plot_graph_2D([x[0] for x in beta_strength_list],
                  [x[1] for x in beta_strength_list],
                  "$\\beta$", "strength",
                  '$\\beta$ v/s strength for $\delta$ = ' + str(delta),
                  'Alternating_Beta_vs_Strength_For_Delta=' + str(delta))
   
    min_beta = 0.0001    
    betas_above_threshold = [b for b, strength in beta_strength_list 
                    if strength > 1]
    if len(betas_above_threshold) > 0:
        min_beta = min(betas_above_threshold)
    print ('Min beta (for delta = {delta}) = {beta}, Strength = {strength}'. 
            format(delta=delta, beta=min_beta, 
                   strength=[strength for b, strength in beta_strength_list 
                    if b == min_beta][0]))
    
    delta_strength_list = get_delta_strengths(sparse_matrices, num_vertices,
                                              beta)
    plot_graph_2D([x[0] for x in delta_strength_list],
                  [x[1] for x in delta_strength_list],
                  "$\delta$", "strength",
                  '$\delta$ v/s strength for $\\beta$ = ' + str(beta),
                  'Alternating_Delta_vs_Strength_For_Beta=' + str(beta))
    
    max_delta = 0.9999
    deltas_above_threshold = [d for d, strength in delta_strength_list 
                              if strength > 1]
    if len(deltas_above_threshold) > 0:
        max_delta = max(deltas_above_threshold)
    print ('Max delta (for beta = {beta}) = {delta}, Strength = {strength}'. 
            format(beta=beta, delta=max_delta, 
                   strength=[strength for d, strength in delta_strength_list 
                    if d == max_delta][0]))
    
if __name__ == '__main__':
    import sys
    
    if len(sys.argv) < 3:
        print 'Usage: python vpm_alternating.py <dataset dirpath> <prefix>'
        sys.exit()
        
    dirpath = sys.argv[1]
    prefix = sys.argv[2]
    
    graphs = load_graphs(dirpath, prefix)
    
    sparse_matrices = []
    for graph in graphs:
        sparse_matrices.append(get_sparse_matrix(graph))
    
    num_vertices = graphs[0].vcount()
    
    BETA_1 = 0.2
    DELTA_1 = 0.7
    print ('Strength for (beta, delta) = ({beta}, {delta}) = {strength}'.
           format(beta=BETA_1, delta=DELTA_1,
                  strength=step_alternating(sparse_matrices, num_vertices, BETA_1, 
                                            DELTA_1)))
    get_beta_delta_variations(sparse_matrices, num_vertices, BETA_1, DELTA_1)
    
    BETA_2 = 0.01
    DELTA_2 = 0.6
    print ('Strength for (beta, delta) = ({beta}, {delta}) = {strength}'.
           format(beta=BETA_2, delta=DELTA_2,
                  strength=step_alternating(sparse_matrices, num_vertices, BETA_2, 
                                            DELTA_2)))
    get_beta_delta_variations(sparse_matrices, num_vertices, BETA_2, DELTA_2)
