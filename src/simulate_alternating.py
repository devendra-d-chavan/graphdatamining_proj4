#!/usr/bin/python

if __name__ == '__main__':
    import sys
    from common import load_graphs, simulate, plot_graph_2D
    
    if len(sys.argv) < 2:
        print 'Usage: python simulate_alternating.py <dataset dirpath> <prefix>'
        sys.exit()
        
    dirpath = sys.argv[1]
    prefix = sys.argv[2]
    
    graphs = load_graphs(dirpath, prefix)
    
    # Simulation
    # -----------
    t = 100
    simulations = 10
    
    BETA_1 = 0.2
    DELTA_1 = 0.7
    
    avg_percent_infected = simulate(graphs, BETA_1, DELTA_1, t, simulations)
    plot_graph_2D(range(t), avg_percent_infected, 'simulation #',
                  'average_percent_infected', 'Average % infected with time',
                  ('Simulation_Alternating_Avg_Percent_Infected_vs_Time_For_Beta={0}_' + 
                   'Delta={1}').format(BETA_1, DELTA_1))
    
    BETA_2 = 0.01
    DELTA_2 = 0.6
    avg_percent_infected = simulate(graphs, BETA_2, DELTA_2, t, simulations)
    plot_graph_2D(range(t), avg_percent_infected, 'simulation #',
                  'average_percent_infected', 'Average % infected with time',
                  ('Simulation_Alternating_Avg_Percent_Infected_vs_Time_For_Beta={0}_' + 
                   'Delta={1}').format(BETA_2, DELTA_2))
