import igraph as ig
from scipy import sparse
from scipy.sparse import linalg
import numpy as np

import matplotlib.pyplot as plt
from mpl_toolkits.mplot3d import Axes3D
from matplotlib import cm

def plot_graph_2D(x, y, xlabel, ylabel, title, filename):
    plt.clf()
    plt.plot(x, y, '^r--', x, [1] * len(x), 'b--')
    plt.xlabel(xlabel)
    plt.ylabel(ylabel)
    plt.title(title)    
    graph_filepath = '../results/plots/' + filename + '.png'
    plt.savefig(graph_filepath, bbox_inches=None, dpi=120)
    
def eff_strength(eval1, beta, delta): 
    return  float(eval1 * beta) / delta

def load_graph(filepath):
    # Create Graph
    graph = None
    static_g_el = []
    with open(filepath) as file_:
        num_vertices, num_edges = file_.readline().strip().split()
        graph = ig.Graph.Load(file_, format='edgelist')
        
        # TODO: Assertion fails for alternating graphs
        # assert int(num_vertices) == graph.vcount()
        # assert int(num_edges) == graph.ecount()
    
    return graph.as_undirected()

def load_graphs(dirpath, prefix):
    import os
    import glob
    
    graphs = []
    os.chdir(dirpath)
    for filename in sorted(glob.glob(prefix + "*")):
        filepath = os.path.join(dirpath, filename)
        graph = load_graph(filepath)
        graphs.append(graph)
        
    return graphs

def get_sparse_matrix(graph):
    adj_mat = graph.get_adjacency().data
    vec_flt = np.vectorize(float)
    adj_mat = vec_flt(adj_mat)
    adj_sparse = sparse.csr_matrix(adj_mat)
    del adj_mat
    
    return adj_sparse

def simulate(graphs, beta, delta, t, simulations):
    
    print 'running simulation with beta: %s, delta: %s' % (beta, delta)
    print '-------------------------------------------'
    
    num_vertices = graphs[0].vcount()
    num_graphs = len(graphs)
    percent_infected = np.zeros((simulations, t))
    avg_percent_infected = []
    c = int(num_vertices / 10)  #     #infected items
    
    for s in range(simulations): 
        print '\nsimulation#: %s' % s
        print '-------------'
        
        rand_int = np.random.random_integers(0, num_vertices - 1, c)
        disease_label_tup = map(lambda x: (x, 'i') 
                                if x in rand_int 
                                else (x, 's'), range(num_vertices))
        disease_label_list = map(lambda x: x[1], disease_label_tup)
    
        infected = filter(lambda x: x[1] == 'i', disease_label_tup)
        infected, tmp = zip(*infected)
        infected = list(infected)
        
        for i in range(t):
            # Choose alternating graphs
            graph = graphs[i % num_graphs]
    # Infect stage - Simulation
    # -------------------------
            # get all the neighbors of infected nodes
            susceptible_neighbors = []
            for v in infected: susceptible_neighbors.extend(graph.neighbors(v))
            # Remove the neighbors that are already infected
            susceptible_neighbors = filter(lambda x: x not in infected,
                                           susceptible_neighbors)
            
            # Generate random # for each neighbor
            rand_no = np.random.rand(len(susceptible_neighbors))
            
            # infection simulation
            cnt = 0
            new_infected = []
            for v in susceptible_neighbors:
                # print 'count: %s, v: %s, rand_prob: %s'%(cnt, v, rand_no[cnt])
                if beta > rand_no[cnt] :    
                    disease_label_list[v] = 'i'
                    new_infected.append(v)
                cnt += 1
                
            # print 'old_l:%s'%len(new_infected)
            new_infected = list(set(new_infected))
            # print 'new_l:%s'%len(new_infected)
            
            
    # Heal Stage - Simulation
    # -----------------------
            rand_no = np.random.rand(len(infected))
            healed, cnt = 0, 0
            
            for v in infected:
                if delta > rand_no[cnt]:
                    disease_label_list[v] = 's'
                    infected.pop(cnt)
                    healed += 1
                    # print 'v removed: %s'%v
                cnt += 1
            
            infected.extend(new_infected)
            
            percent_infected_val = float(len(infected)) / num_vertices
            percent_infected[s, i] = percent_infected_val 
            print (('time step: {0},\t # new infected items: {1},\t ' + 
                   '# items healed: {2},\t percent infected: {3}')
                   .format(i, len(new_infected), healed, percent_infected_val))
    
    avg_percent_infected = [percent_infected[:, i].mean() for i in range(t)]
    return avg_percent_infected
