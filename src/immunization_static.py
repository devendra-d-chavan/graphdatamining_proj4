#!/usr/bin/python

import igraph as ig
from scipy import sparse
from scipy.sparse import linalg
import numpy as np

from common import *

def apply_policy_A(graph, k):
    # Policy A: Select k nodes at random for immunization
    import random
    
    vertices_to_remove = set()
    num_vertices = graph.vcount()
    
    while(len(vertices_to_remove) < k):
        vertexid = random.randint(0, num_vertices - 1)
        vertices_to_remove.add(vertexid)
        
    graph.delete_vertices(ig.VertexSeq(graph, vertices_to_remove))
    return graph

def apply_policy_B(graph, k):
    # Policy B: Select k highest degree nodes for immunization
    node_degrees = graph.degree()
    sorted_indices = np.argsort(node_degrees)
    
    # Remove the top k highest degree vertices
    vertices_to_remove = [int(x) for x in sorted_indices[graph.vcount() - k:]]
    graph.delete_vertices(ig.VertexSeq(graph, vertices_to_remove))
    return graph

def apply_policy_C(graph, k):
    # Policy C: Select highest degree node k times for immunization
    for i in range(0, k):
        node_degrees = graph.degree()
        vertex_to_remove = np.argsort(node_degrees)[len(node_degrees) - 1]
        graph.delete_vertices(ig.VertexSeq(graph, [int(vertex_to_remove)]))
        
    return graph

def apply_policy_D(graph, k, eigen_vector):
    # Policy D: Select the k highest corresponding nodes in the eigen vector 
    # corresponding to the largest eigen value  
    temp = eigen_vector
    temp.sort(reverse=True)
    
    # Get the k largest value
    k_largest_value = temp[k - 1]
    
    vertices_to_remove = []
    i = 0
    for i in range(0, len(eigen_vector)):
        if eigen_vector[i] >= k_largest_value:
             vertices_to_remove.append(i)
             
    graph.delete_vertices(ig.VertexSeq(graph, vertices_to_remove))
    return graph

def step_immunization(filepath, k, beta, delta, apply_policy,
                      run_simulation=False):
    graph = load_graph(filepath)
    
    num_vertices = graph.vcount()
    if k >= num_vertices:
        return 0
    
    if apply_policy == apply_policy_D:
        adj_sparse = get_sparse_matrix(graph)
        _, eigen_vector = linalg.eigsh(adj_sparse, k=1, which='LA',
                                             return_eigenvectors=True)
        eigen_vector = [x[0] for x in eigen_vector]
        graph = apply_policy(graph, k, eigen_vector)
    else:
        graph = apply_policy(graph, k)
        
    adj_sparse = get_sparse_matrix(graph)
    
    if run_simulation:
        policy_name = None
        if apply_policy == apply_policy_A:
            policy_name = 'Policy_A'
        elif apply_policy == apply_policy_B:
            policy_name = 'Policy_B'
        elif apply_policy == apply_policy_C:
            policy_name = 'Policy_C'
        elif apply_policy == apply_policy_D:
            policy_name = 'Policy_D'
    
        t = 100
        simulations = 10
        avg_percent_infected = simulate([graph], beta, delta, t, simulations)
        plot_graph_2D(range(t), avg_percent_infected, 'simulation #',
                 'average_percent_infected', 'Average % infected with time',
                 ('Simulation_{0}_Immunized_Static_Avg_Percent_Infected_vs_' + 
                  'Time_For_Beta={2}_Delta={2}').format(policy_name, beta,
                                                        delta))
    
    eigen_value = linalg.eigsh(adj_sparse, k=1, which='LA',
                         return_eigenvectors=False)[-1]
    return eff_strength(eigen_value, beta, delta)

def vary_k(filepath, beta, delta, strength, apply_policy):
    NUM_INTERVALS = 20
    interval_size = 10
    interval_start = 0
    
    if strength > 1:
        # Try with 20 interval size
        interval_start = interval_start + interval_size * NUM_INTERVALS
        interval_size = 20
         
        strength = step_immunization(filepath,
                                         interval_start + 
                                         NUM_INTERVALS * interval_size,
                                         beta, delta, apply_policy)
         
        if strength > 1:
            # Try with 50 interval size
            interval_start = interval_start + NUM_INTERVALS * interval_size
            interval_size = 50
            
            strength = step_immunization(filepath,
                                         interval_start + 
                                         NUM_INTERVALS * interval_size,
                                         beta, delta, apply_policy)
            
            if strength > 1:
                # Try with 100 interval size
                interval_start = interval_start + NUM_INTERVALS * interval_size
                interval_size = 100
                
                strength = step_immunization(filepath,
                                         interval_start + 
                                         NUM_INTERVALS * interval_size,
                                         beta, delta, apply_policy)
                
                if strength > 1:
                    # Try with 200 interval size
                    interval_start = (interval_start + 
                                      NUM_INTERVALS * interval_size)
                    interval_size = 200
    
    k_strength_tuples = [] 
    for k in range(interval_start, interval_start + 
                                    NUM_INTERVALS * interval_size + 1,
                                    interval_size):
        strength = step_immunization(filepath, k, beta, delta, apply_policy)
        k_strength_tuples.append((k, strength))
        
    return k_strength_tuples
    
if __name__ == '__main__':
    import sys
    
    if len(sys.argv) < 2:
        print 'Usage: python immunization_static.py <graph filepath>'
        sys.exit()
        
    filepath = sys.argv[1]
    
    K1 = 200
    BETA_1 = 0.2
    DELTA_1 = 0.7
    
    print '(beta, delta) = ({beta}, {delta}), K = {k}'.format(beta=BETA_1,
                                                              delta=DELTA_1,
                                                              k=K1)
    
    strength_A = step_immunization(filepath, K1, BETA_1, DELTA_1,
                                   apply_policy_A, True)
    strength_B = step_immunization(filepath, K1, BETA_1, DELTA_1,
                                   apply_policy_B, True)
    strength_C = step_immunization(filepath, K1, BETA_1, DELTA_1,
                                   apply_policy_C, True)
    strength_D = step_immunization(filepath, K1, BETA_1, DELTA_1,
                                   apply_policy_D, True)
    
    print (('Strength Policy A : {0}\nStrength Policy B : {1}\n' + 
            'Strength Policy C : {2}\nStrength Policy D : {3}\n')
           .format(strength_A, strength_B, strength_C, strength_D))
    
    print 'Plotting k vs strength_A...'
    k_strength_tuples = vary_k(filepath, BETA_1, DELTA_1, strength_A,
                               apply_policy_A)
    plot_graph_2D([k for k, _ in k_strength_tuples],
                  [strength for _, strength in k_strength_tuples],
                  "k", "strength",
                  'Policy A - k v/s strength for $\\beta$ = ' + str(BETA_1) + 
                  ' and $\delta$ = ' + str(DELTA_1),
                  'Immunization_Static_PolicyA')
    
    print 'Plotting k vs strength_B...'
    k_strength_tuples = vary_k(filepath, BETA_1, DELTA_1, strength_B,
                               apply_policy_B)
    plot_graph_2D([k for k, _ in k_strength_tuples],
                  [strength for _, strength in k_strength_tuples],
                  "k", "strength",
                  'Policy B - k v/s strength for $\\beta$ = ' + str(BETA_1) + 
                  ' and $\delta$ = ' + str(DELTA_1),
                  'Immunization_Static_PolicyB')
    
    print 'Plotting k vs strength_C...'
    k_strength_tuples = vary_k(filepath, BETA_1, DELTA_1, strength_C,
                               apply_policy_C)
    plot_graph_2D([k for k, _ in k_strength_tuples],
                  [strength for _, strength in k_strength_tuples],
                  "k", "strength",
                  'Policy C - k v/s strength for $\\beta$ = ' + str(BETA_1) + 
                  ' and $\delta$ = ' + str(DELTA_1),
                  'Immunization_Static_PolicyC')
    
    print 'Plotting k vs strength_D...'
    k_strength_tuples = vary_k(filepath, BETA_1, DELTA_1, strength_D,
                               apply_policy_D)
    plot_graph_2D([k for k, _ in k_strength_tuples],
                  [strength for _, strength in k_strength_tuples],
                  "k", "strength",
                  'Policy D - k v/s strength for $\\beta$ = ' + str(BETA_1) + 
                  ' and $\delta$ = ' + str(DELTA_1),
                  'Immunization_Static_PolicyD')    
