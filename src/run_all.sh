#!/bin/sh

echo "Static network"
echo ""

echo "Calculating strength..."
time python vpm_static.py ../datasets/static.network

echo ""
echo "Running simulation..." 
time python simulate_static.py ../datasets/static.network

echo ""
echo "Running immunization policy..."
time python immunization_static.py ../datasets/static.network

echo ""
echo "Time varying network"
echo ""

echo "Calculating strength..."
time python vpm_alternating.py ../datasets alternating

echo ""
echo "Running simulation..." 
time python simulate_alternating.py ../datasets alternating

echo ""
echo "Running immunization policy..."
time python immunization_alternating.py ../datasets alternating
