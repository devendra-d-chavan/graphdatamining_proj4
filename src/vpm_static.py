#!/usr/bin/python

import igraph as ig
from scipy import sparse
from scipy.sparse import linalg
import numpy as np

from common import *

def get_min_beta_strength(eval1, delta):
    for i in range(0, 100):
        beta = 0.01 * i
        strength = eff_strength(eval1, beta, delta)
        if strength > 1:
            return beta, strength
        
    return 1 * 0.01, eff_strength(eval1, 1 * 0.01, delta)
        
def get_max_delta_strength(eval1, beta):
    for i in reversed(range(1, 100)):
        delta = 0.01 * i
        strength = eff_strength(eval1, beta, delta)
        if strength > 1:
            return delta, strength
        
    return 99 * 0.01, eff_strength(eval1, beta, 99 * 0.01)

def step_static(eigen_value, beta, delta):
    print ('Strength for (beta, delta) = ({beta}, {delta}) = {strength}'.
           format(beta=beta, delta=delta,
                  strength=eff_strength(eigen_value, beta, delta)))
    
    min_beta, strength = get_min_beta_strength(eigen_value, delta)
    print ('Min beta (for delta = {delta}) = {beta}, Strength = {strength}'. 
            format(delta=delta, beta=min_beta, strength=strength))
    
    max_delta, strength = get_max_delta_strength(eigen_value, beta)
    print ('Max delta (for beta = {beta}) = {delta}, Strength = {strength}'. 
            format(beta=beta, delta=max_delta, strength=strength))
    
def plot_graphs(eigen_value, beta, delta):
    vec_eff_strength = np.vectorize(eff_strength)
    
    # 2-D plots delta fixed
    beta_var = np.arange(0.001, 0.05, 0.001)
    strength = vec_eff_strength(eigen_value, beta_var, delta)
    plot_graph_2D(beta_var, strength, 'Beta, $\\beta$', 'Effective strength, s',
                  ('Plot of effective strength v/s' + ' $\\beta$' + ' and ' + 
                  '$\delta = ' + str(delta) + '$'),
                  'Static_Effective_strength_vs_Beta_For_Delta' + str(delta))
    
    # 2-D plots beta fixed
    delta_var = np.arange(0.5, 1, 0.025)
    strength = vec_eff_strength(eigen_value, beta, delta_var)
     
    plot_graph_2D(delta_var, strength, 'Delta, $\delta$', 'Effective strength, s',
                  ('Plot of effective strength v/s' + ' $\delta$' + ' and ' + 
                  '$\\beta = ' + str(beta) + '$'),
                  'Static_Effective_strength_vs_Delta_For_Beta' + str(beta))
    
         
#     vec_eff_strength = np.vectorize(eff_strength)
#     # 3-D plot of beta versus delta
#     # -----------------------------
#     fig1 = plt.figure(1)
#     ax = fig1.gca(projection='3d')
#     beta = np.arange(0.05, 1, 0.05)
#     delta = np.arange(0.5, 1, 0.025)
#     beta, delta = np.meshgrid(beta, delta)
#      
#     strength = vec_eff_strength(eval1, beta, delta)
#     surf = ax.plot_surface(beta, delta, strength, rstride=1, cstride=1, cmap=cm.coolwarm, linewidth=0, antialiased=False)
#     ax.set_xlabel('beta')
#     ax.set_ylabel('delta')
#     ax.set_zlabel('effective_strength')
#     # title = 'Plot of effective strength v/s' + '$ \beta & \delta$'
#     ax.set_title('Plot of effective strength v/s' + ' $ \\beta & \delta$')
#     # plt.text(.055, .0095, '$\lambda \in$ (0,1) & Penalty:%.1e' %pen)
#     plt.show()
#  
    
if __name__ == '__main__':
    import sys
    
    if len(sys.argv) < 2:
        print 'Usage: python vpm_static.py <graph filepath>'
        sys.exit()
        
    filepath = sys.argv[1]
    graph = load_graph(filepath) 
    
    adj_sparse = get_sparse_matrix(graph)
    
    eigen_value = linalg.eigsh(adj_sparse, k=1, which='LA',
                         return_eigenvectors=False)[-1]
    print 'Largest eigenvalue : {0}'.format(eigen_value)
    
    # For beta1 and delta1
    BETA_1 = 0.2
    DELTA_1 = 0.7
    step_static(eigen_value, BETA_1, DELTA_1)
    plot_graphs(eigen_value, BETA_1, DELTA_1)
    
    # For beta2 and delta2
    BETA_2 = 0.01
    DELTA_2 = 0.6
    step_static(eigen_value, BETA_2, DELTA_2)
    plot_graphs(eigen_value, BETA_2, DELTA_2)
